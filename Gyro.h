/**
* @copyright 2019 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

#ifndef H_MAIN_GYRO
#define H_MAIN_GYRO

class Gyro
{
    private:
        int accValue[3] = {0,0,0};
        int gyroValue[3] = {0,0,0};
        int temperature = 0;
        int accCorr = 0;
        float gyroCorr = 0.0f;

        u32 timeLastMeasurement = 0.0f;

        int accAngleOffset[3] = {0,0,0};
    public:
        int accAngle[3] = {0,0,0};
        float gyroAngle[3] = {0.0f, 0.0f, 0.0f};

        void init();
        void calibrate();
        void measure();
};

#endif
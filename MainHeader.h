/**
* @copyright 2019 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

#ifndef H_MAIN_HEADER
#define H_MAIN_HEADER

// Enables serial output for sensor debugging
//#define SERIAL_OUTPUT

#include "Types.h"
#include "Gyro.h"
#include "Screen.h"
#include "Particle.h"
#include "ParticleField.h"

#endif
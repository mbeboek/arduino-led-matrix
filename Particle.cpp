#include "MainHeader.h"

#define DAMP_VELOCITY(velocity, perc) velocity[i] = (velocity[i] * PARTICLE_BOUNCE_DAMP) / perc;

void Particle::update(s16 gravity[2], ParticleField &particleField)
{
    for(int i=0; i<2; ++i)
    {
        lastPos[i] = pos[i];
        velocity[i] += gravity[i];    
        pos[i] += velocity[i];
        screenPos[i] = pos[i] / PARTICLE_SCALAR;

        //velocity[i] = (velocity[i] * 99) / 100;
    }
    // check if pixel already  has a particle
    if(particleField.checkParticleBuffer(screenPos[0], screenPos[1])) 
    {
        for(int i=0; i<2; ++i)
        {
            DAMP_VELOCITY(velocity, 100)
            pos[i] = lastPos[i];
            screenPos[i] = pos[i] / PARTICLE_SCALAR;
        }
    }
}

void Particle::draw(Screen &screen)
{
    screen.setPixel(screenPos[0], screenPos[1], 1);
}
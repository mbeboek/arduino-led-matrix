/**
* @copyright 2019 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

#ifndef H_PARTICLE_FIELD
#define H_PARTICLE_FIELD

#define PARTICLE_FIELD_MAX_PARTICLES 128
#define PARTICLE_FIELD_GRID_SIZE 32
#define PARTICLE_FIELD_BUFFER_DEF_VAL 0xFF

class ParticleField
{
    private:
        int particleCount;
        Particle particles[PARTICLE_FIELD_MAX_PARTICLES];

        s16 gravity[2] = {0, 0};
        u8 particleBuffer[PARTICLE_FIELD_GRID_SIZE][PARTICLE_FIELD_GRID_SIZE];

    public:
        ParticleField(int count)
        : particleCount(count)
        {
            memset(
                particleBuffer, 
                PARTICLE_FIELD_BUFFER_DEF_VAL, 
                PARTICLE_FIELD_GRID_SIZE * PARTICLE_FIELD_GRID_SIZE
            );
        }

        void spawnParticles();
        void updateAndDraw(Screen &screen);

        bool checkParticleBuffer(u8 x, u8 y);

        void setGravity(s16 x, s16 y);
        void addGravity(s16 x, s16 y);
        s16 getGravity(int component);
};

#endif
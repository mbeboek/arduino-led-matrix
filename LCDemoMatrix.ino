/**
* @copyright 2019 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

#include "MainHeader.h"

auto screen = Screen(
  2, 3, 4,
  5, 6, 7,
  8, 9, 10,
  11, 12, 13
);

auto particleField = ParticleField(128);
auto gyro = Gyro();

void setup() 
{
  #ifdef SERIAL_OUTPUT
    Serial.begin(9600);
  #endif
  Serial.begin(9600);

  screen.clear();
  particleField.setGravity(-35, 60);
  particleField.spawnParticles();

  gyro.init();
}

void loop() 
{ 
  gyro.measure();
  particleField.setGravity(
    -((s16)gyro.accAngle[1]) * 10 / 8,  
    ((s16)gyro.accAngle[0]) * 10 / 8
  );


  auto t = micros();
  particleField.updateAndDraw(screen);
  t = micros() - t;

  Serial.print("update: ");
  Serial.print(t);
  Serial.print("\t\t");

  auto td = micros();
  screen.draw();
  td = micros() - td;

  Serial.print("draw: ");
  Serial.print(td);
  Serial.print("\n");
  //delay(5);
}

/**
* @copyright 2019 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

#include "MainHeader.h"

void ParticleField::spawnParticles()
{
    for(int i=0; i<particleCount; ++i)
    {
        particles[i] = Particle(i, (i%32), (i/32), 0,0);
        particleBuffer[particles[i].screenPos[1]][particles[i].screenPos[0]] = i;
    }
}

void ParticleField::updateAndDraw(Screen &screen)
{
    for(int i=0; i<particleCount; ++i)
    {    
        particleBuffer[particles[i].screenPos[1]][particles[i].screenPos[0]] = PARTICLE_FIELD_BUFFER_DEF_VAL;

        particles[i].update(gravity, *this);

        particleBuffer[particles[i].screenPos[1]][particles[i].screenPos[0]] = i;
    }

    for(int i=0; i<particleCount; ++i)
    {
        particles[i].draw(screen);
    }
/*
    screen.draw();
    delay(20);

    for(int y=0; y<32; ++y)
    {
        for(int x=0; x<32; ++x)
            screen.setPixel(x, y, (particleBuffer[y][x] != PARTICLE_FIELD_BUFFER_DEF_VAL) ? 1 : 0);
    }*/
}

bool ParticleField::checkParticleBuffer(u8 x, u8 y)
{
    if(x >= PARTICLE_FIELD_GRID_SIZE || y >= PARTICLE_FIELD_GRID_SIZE)
        return true;

    return particleBuffer[y][x] != PARTICLE_FIELD_BUFFER_DEF_VAL;
}

void ParticleField::setGravity(s16 x, s16 y)
{
    gravity[0] = x;
    gravity[1] = y;
}

void ParticleField::addGravity(s16 x, s16 y)
{
    gravity[0] += x;
    gravity[1] += y;
}

s16 ParticleField::getGravity(int component)
{
    return gravity[component];
}
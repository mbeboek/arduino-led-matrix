/**
* @copyright 2019 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

#ifndef H_PARTICLE
#define H_PARTICLE

#define PARTICLE_SCALAR 1024
#define PARTICLE_BOUNCE_DAMP 20
#define PARTICLE_MIN_VELOCITY 50

#define PARTICLE_GRID_MAX_POS 31

class ParticleField;

class Particle
{
  private:
    friend class ParticleField;

    u16 id;
    s16 lastPos[2];
    s16 pos[2];
    u8 screenPos[2];
    s16 velocity[2];

  public:

    Particle(u16 id, u8 px, u8 py, s16 vx, s16 vy)
      : id(id),
        pos{px * PARTICLE_SCALAR, py * PARTICLE_SCALAR}, 
        lastPos{pos[0], pos[1]},
        screenPos{px, py}, velocity{vx, vy}
    {}

    Particle() : Particle(0,0,0,0,0) {}

    void update(s16 gravity[2], ParticleField &particleField);
    void draw(Screen &screen);
};

#endif
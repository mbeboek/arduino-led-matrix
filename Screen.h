/**
* @copyright 2019 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

#ifndef H_SCREEN
#define H_SCREEN

#define NUM_PANLES 4
#define NUM_PANEL_SCREENS 4
#define NUM_PANEL_SCREEN_ROWS 32

#include <MD_MAX72xx.h>

#define SCREEN_HARDWARE_TYPE MD_MAX72XX::GENERIC_HW

class Screen
{
    private:
        MD_MAX72XX panels[NUM_PANLES];

        byte dataA[NUM_PANLES][NUM_PANEL_SCREEN_ROWS]; // rows x columns, 128 bytes
        byte dataB[NUM_PANLES][NUM_PANEL_SCREEN_ROWS];

        byte (*dataCurrent)[NUM_PANEL_SCREEN_ROWS];
        byte (*dataLast)[NUM_PANEL_SCREEN_ROWS];

        void switchBuffer();

    public:
    	Screen(
            int din0, int clk0, int cs0,
            int din1, int clk1, int cs1,
            int din2, int clk2, int cs2,
            int din3, int clk3, int cs3
        ) : panels{
                MD_MAX72XX(SCREEN_HARDWARE_TYPE, din0, clk0, cs0, NUM_PANEL_SCREENS), // DIN, CLK, CS
                MD_MAX72XX(SCREEN_HARDWARE_TYPE, din1, clk1, cs1, NUM_PANEL_SCREENS), // DIN, CLK, CS
                MD_MAX72XX(SCREEN_HARDWARE_TYPE, din2, clk2, cs2, NUM_PANEL_SCREENS), // DIN, CLK, CS
                MD_MAX72XX(SCREEN_HARDWARE_TYPE, din3, clk3, cs3, NUM_PANEL_SCREENS) // DIN, CLK, CS
            }
        {
            dataCurrent = dataA;
            dataLast = dataB;
        }

        void setPixel(u8 x, u8 y, u8 value);
        u8 getPixel(u8 x, u8 y);

        void draw();
        void clear();
};

#endif

/**
* @copyright 2019 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

#ifndef H_TYPES
#define H_TYPES

typedef unsigned char u8;
typedef signed char s8;

//typedef unsigned int u16;
typedef signed int s16;

typedef unsigned long u32;
typedef signed long s32;

#endif
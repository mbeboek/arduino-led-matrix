/**
* @copyright 2019 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

#include "MainHeader.h"

void Screen::setPixel(u8 x, u8 y, u8 value)
{
    if(x > 31 || y > 31)return;

    int panel = y / 8; // which of the 4 main panel is the pixel on
    int row = (y % 8) + ((x / 8) * 8); // which row inside the panel (goes across al screens)
    int bitVal = 1 << (7-(x % 8)); // value, modulo-8 to compensate rows that are going across screens

    if(value) {
        dataCurrent[panel][row] |= bitVal;
    } else {
        dataCurrent[panel][row] &= ~bitVal;
    }
}

u8 Screen::getPixel(u8 x, u8 y)
{
    if(x > 31 || y > 31)return;

    int panel = y / 8; // which of the 4 main panel is the pixel on
    int row = (y % 8) + ((x / 8) * 8); // which row inside the panel (goes across al screens)

    return dataCurrent[panel][row] & (1 << (7-(x % 8)));
}

void Screen::draw()
{
    for(int panel=0; panel<NUM_PANLES; ++panel)
    {
        for(int row=0; row<NUM_PANEL_SCREEN_ROWS; ++row)
        {
            if(dataCurrent[panel][row] != dataLast[panel][row])
                panels[panel].setColumn(row, dataCurrent[panel][row]);
        }
    }

    switchBuffer();
}

void Screen::switchBuffer()
{
    byte (*oldBuffer)[NUM_PANEL_SCREEN_ROWS] = dataCurrent;
    dataCurrent = dataLast;
    dataLast = oldBuffer;

    memset(dataCurrent, 0, NUM_PANLES * NUM_PANEL_SCREEN_ROWS);
}

void Screen::clear()
{
    for(int panel=0; panel<NUM_PANLES; ++panel)
    {
        panels[panel].begin();
        panels[panel].update(MD_MAX72XX::ON);
        panels[panel].control(MD_MAX72XX::INTENSITY , 1);
        panels[panel].clear();
    }

    memset(dataA, 0, NUM_PANLES * NUM_PANEL_SCREEN_ROWS);
    memset(dataB, 0, NUM_PANLES * NUM_PANEL_SCREEN_ROWS);
}